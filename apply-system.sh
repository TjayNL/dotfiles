#!/bin/sh
pushd ~/.dotfiles/nixos
sudo nixos-rebuild switch -I nixos-config=./system/configuration.nix
popd
