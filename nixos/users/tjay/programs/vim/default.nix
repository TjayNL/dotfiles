{ self, config, pkgs, lib, ... }:

let
  # Helper function to add plugins directly from GitHub if they are not
  # packaged in nixpkgs yet
  plugin = name: repo: branch: sha256:
    pkgs.vimUtils.buildVimPluginFrom2Nix {
      pname = "vim-plugin-${name}";
      version = "git";
      src = builtins.fetchGit {
        url = "https://github.com/${repo}.git";
        ref = branch;
        rev = sha256;
      };
    };

in {
  home.file.".vimrc".source = ./.vimrc;
  
  programs.neovim = {
    enable = true;
    viAlias = true;
    vimAlias = true;
    vimdiffAlias = true;
    withNodeJs = true;

    # Plugins
    plugins = with pkgs.vimPlugins; [
      ale
      commentary
      coc-highlight
      coc-nvim
      CSApprox
      ctrlp-vim
      (plugin "deLimitMate" "Raimondi/delimitMate" "master" "537a1da0fa5eeb88640425c37e545af933c56e1b" )
      emmet-vim
      fugitive
      (plugin "fzf" "junegunn/fzf" "master" "4cd621e877cb3a8e44b12ba3a7ce58709862922f" )
      # (plugin "fzf.vim" "junegunn/fzf" "master" "a88311b222eb9f90fa9fa72292e61d15c6767866" )
      indentLine
      jedi-vim
      nerdtree
      (plugin "phpactor" "phpactor/phpactor" "master" "09436669a45ea6733eb81a00dc3f83a7d6db76c9")
      (plugin "requirements.txt.vim" "raimon49/requirements.txt.vim" "master" "ac7f865672a34a090981b77e17cbb9f95ac61850")
      rainbow
      tabular
      tagbar
      (plugin "vimproc.vim" "shougo/vimproc.vim" "master" "8f40d86ab938d5df8c1c9824320621ae9f0d5609")
      vim-airline
      vim-airline-themes
      # vim-bootstrap-updater
      (plugin "vim-coloresque" "gko/vim-coloresque" "master" "e12a5007068e74c8ffe5b1da91c25989de9c2081")
      (plugin "vim-css3-syntax" "hail2u/vim-css3-syntax" "main" "246cc6e0992c941ad0d0162bc0a795e6d9bca286" )
      vim-devicons
      vim-gitgutter
      (plugin "vim-haml" "tpope/vim-haml" "master" "467d0ee60a66387c1f451ad4795a3cca4d1ed235")
      vim-hybrid-material
      vim-javascript-syntax
      vim-rhubarb
      vim-markdown
      vim-misc
      vim-nix
      vim-one
      # vim-php-cs-fixer
      vim-projectionist
      vim-rails
      (plugin "vim-rake" "tpope/vim-rake" "master" "34ece18ac8f2d3641473c3f834275c2c1e072977")
      (plugin "vim-rspec" "thoughtbot/vim-rspec" "master" "52a72592b6128f4ef1557bc6e2e3eb014d8b2d38")
      (plugin "vim-ruby-refactoring" "ecomba/vim-ruby-refactoring" "main" "6447a4debc3263a0fa99feeab5548edf27ecf045")
      (plugin "vim-session" "xolox/vim-session" "master" "9e9a6088f0554f6940c19889d0b2a8f39d13f2bb")
      vim-snippets
    ];
    extraConfig = ''
      if filereadable($HOME . "/.vimrc")
        source $HOME/.vimrc
      endif
    '';
  };
}
