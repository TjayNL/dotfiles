{
  xdg.configFile."networkmanager-dmenu/config.ini".text = ''
    [dmenu]
    dmenu_command = rofi
    rofi_highlight = True
    rofi.theme = theme.rofi
    [editor]
    gui_if_available = True
  '';

  # services.network-manager-applet.enable = False;
}

