{ config, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in
{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  imports = (import ./programs);

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "tjay";
  home.homeDirectory = "/home/tjay";

  nixpkgs.config.allowUnfree = true;
 
  home.packages = with pkgs; [
    # System Essentials
    git
    git-crypt
    gnupg
    killall
    p7zip
    pywal
    python38Packages.pywal
    shared_mime_info
    unstable.nodejs

    # Themes
    pop-gtk-theme
    pop-icon-theme

    # Terminal Apps
    vim
    htop
    neofetch
    ranger
    appimage-run

    # GUI Apps
    gitkraken
    geany
    gnome3.networkmanagerapplet
    gnome3.gnome-disk-utility
    leafpad
    lxappearance
    networkmanager_dmenu
    pavucontrol
    pcmanfm
    spotify
    xarchiver

    # Work Apps
    gimp
    inkscape
    vscode-with-extensions
  ];

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.03";
}
