# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in
{ 
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./gaming.nix
      ./wm/qtile.nix
    ];

  #
  # BOOT SETTINGS
  #

  boot = {
    # Use the systemd-boot EFI boot loader
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    # Select Kernel
    kernelPackages = pkgs.linuxPackages_latest;

    initrd.kernelModules = [ "amdgpu" ];
  };

  #
  # HARDWARE
  #

  hardware = {
    cpu.amd.updateMicrocode = true;
    pulseaudio.enable = true;
    opengl = {
      enable = true;
      driSupport = true;
      package = pkgs.mesa.drivers;
      driSupport32Bit = true;
      package32 = pkgs.pkgsi686Linux.mesa.drivers;
    };
  };

  #
  # NETWORK SETTINGS
  #

  networking = {
    hostName = "nixos"; # Define your hostname.
    networkmanager.enable = true;
    #wireless.enable = true; # Enables wireless support via wpa_supplicant.

    # The global useDHCP flag is deprecated, therefore explicitly set to false here.
    # Per-interface useDHCP will be mandatory in the future, so this generated config
    # replicates the default behaviour.
    useDHCP = false;
    interfaces.enp3s0.useDHCP = true;

    # Configure network proxy
    # proxy = {
    #   default = "https://user:password@proxy:port/";
    #   noProxy = "127.0.0.1,localhost,internal.domain";
    # };

    # Open ports in the firewall.
    # firewall.allowedTCPPorts = [ ... ];
    # firewall.allowedUDPPorts = [ ... ];
    # Or disable the firewall altogether.
    # firewall.enable = false;

  };
  
  #
  # LOCALE SETTINGS
  #

  # Set your time zone.
  time.timeZone = "Europe/Amsterdam";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };


  #
  # SERVICES SETTINGS
  #
  
  sound.enable = true; 

 
  # Pipewire
  # security.rtkit.enable = true;
  # services.pipewire = {
  #   enable = true;
  #   alsa.enable = true;
  #   alsa.support32Bit = true;
  #   pulse.enable = true;
  #   # jack.enable = true;
  # };

  # Xserver settings
  services.xserver = {
    enable = true; # Enable the X11 windowing system.
    videoDrivers = [ "amdgpu" ];
    # layout = "us";
    # xkbOptions = "eurosign:e";

    displayManager = {
      lightdm.enable = true;
      defaultSession = "none+qtile";
    };

    windowManager = {
      qtile.enable = true;
    };
  };

  # Auto mount external drives
  services.gvfs = {
    enable = true;
  };

  services.flatpak = {
    enable = true;
  };

  # Needed for Flatpaks
  xdg.portal.enable = true;
  
  # Misc services
  services = {
    # Enable CUPS to print documents.
    # printing.enable = true;
    
    # Enable the OpenSSH daemon.
    # openssh.enable = true;
  };

  #
  # PROGRAMS
  #

  programs.zsh = {
    enable = true;
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr = {
  #   enable = true;
  # };

  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # }; 

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.tjay = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.zsh;
  };

  #
  # FONTS
  #
  fonts = {
    fonts = with pkgs; [
      font-awesome
      powerline-fonts
      fira
      fira-code
      fira-code-symbols
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      montserrat
    ];
  };

  #
  # System Packages
  #

  # Allow Unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    arandr
    git
    lxmenu-data
    nix-prefetch-github
    p7zip
    oh-my-zsh
    udisks
    udiskie
    unrar
    unzip
    vim
    wget
    zsh
    zsh-nix-shell
  ];
  
  #
  # Nix settings
  #

  # Optimize storage
  nix = {
    # Automate `nix-store --optimise`
    autoOptimiseStore = true;
    
    # Automate garbage collection
    gc = {
      automatic = true;
      dates     = "weekly";
      options   = "--delete-older-than 7d";
    };

    # Avoid unwanted garbage collection when using `nix-direnv`
    # extraOptions = ''
    #   keep-outputs     = true;
    #   keep-derivations = true;
    # '';

    # Required by Cashix to be used as non-root user
    trustedUsers = [ "root" "tjay" ];
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?

}

