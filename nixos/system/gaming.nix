{ config, lib, pkgs, ... }:

let
  unstable = import <nixos-unstable> { inherit (config.nixpkgs) config; };
in
{
  # nixpkgs.config.allowUnfreePredicate = unstable: builtins.elem (lib.getName unstable) [
  #   "steam"
  #   "steam-original"
  #   "steam-runtime"
  # ];
  
  # nixpkgs.overlays = [
  #   (self: super: { inherit (unstable) steam; })
  # ];
  
  #
  # HARDWARE
  #

  hardware.steam-hardware = {
    enable = true;
  };

  #
  # PROGRAMS
  #

  # programs.steam = {
  #   enable = true;
  # };

  #
  # Packages
  #

  environment.systemPackages = with pkgs; [
    lutris
    discord
  ];
}
