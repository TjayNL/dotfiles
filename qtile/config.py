from typing import List  # noqa: F401

import os
import re
import socket
import subprocess

from libqtile import  bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Match, Key, KeyChord, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
alt = "mod1"
terminal = "alacritty"

keys = [
    # Switch between windows in current stack pane
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),

    # Move windows up or down in current stack
    Key([mod, "shift"], "h", lazy.layout.swap_left()),
    Key([mod, "shift"], "l", lazy.layout.swap_right()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),

    #
    KeyChord([mod], "w", [
        Key([], "g", lazy.layout.grow()),
        Key([], "s", lazy.layout.shrink()),
        Key([], "n", lazy.layout.normalize()),
        Key([], "m", lazy.layout.maximize())],
        mode="Windows"
    ),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next()),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key([mod], "a", lazy.spawn("rofi -combi-modi window,drun -show combi -modi combi -theme slate -show-icons -lines 5")),

    # Open terminal
    Key([mod], "t", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Launch Terminal Apps
    Key(["control", alt], "v", lazy.spawn(terminal + " -e vim"), desc="Launch vim in terminal"),
    Key(["control", alt], "r", lazy.spawn(terminal + " -e ranger"), desc="Launch ranger in terminal"),
    Key(["control", alt], "h", lazy.spawn(terminal + " -e htop"), desc="Launch htop in terminal"),

    # Launch Programs
    Key([mod], "e", lazy.spawn("geany")),
    Key([mod], "w", lazy.spawn("firefox")),
    Key([mod], "f", lazy.spawn("pcmanfm")),
    Key([mod], "g", lazy.spawn(terminal + " appimage-run ~/Applications/GravitDesigner.AppImage")),


    # Change the volume if your keyboard has special volume keys.
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -c 0 -q set Master 2dB+")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -c 0 -q set Master 2dB-")),
    Key([], "XF86AudioMute", lazy.spawn("amixer -c 0 -q set Master toggle")),

    # Also allow changing volume the old fashioned way.
    Key([mod], "equal", lazy.spawn("amixer -c 0 -q set Master 2dB+")),
    Key([mod], "minus", lazy.spawn("amixer -c 0 -q set Master 2dB-"))
]

groups = [
    Group("1", label="1"),
    Group("2", label="2"),
    Group("3", label="3"),
    Group("4", label="4", matches=[Match(wm_class=["Steam"])]),
    Group("5", label="5", matches=[Match(wm_class=["discord"])]),
]


for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])


layout_theme = {
    "margin":10,
    "border_width":2,
    "border_focus": "#FFC700",
    "border_normal": "#4DD0E1"
}

layouts = [
    layout.Max(**layout_theme),
    layout.Stack(num_stacks=2, **layout_theme),
    # layout.Bsp(**layout_theme),
    # layout.Columns(**layout_theme),
    # layout.Matrix(**layout_theme),
    layout.MonadTall(**layout_theme),
    # layout.MonadWide(**layout_theme),
    layout.RatioTile(**layout_theme),
    # layout.Tile(**layout_theme),
    # layout.TreeTab(**layout_theme),
    # layout.VerticalTile(**layout_theme),
    # layout.Zoomy(**layout_theme),
]

colors = [
    ["#C4C7C5", "#C4C7C5"], # (0) Foreground color
    ["#4DD0E1", "#4DD0E1"], # (1) Accent color
    ["#FFC700", "#FFC700"], # (2) Yellow
    ["#42A5F5", "#42A5F5"], # (3) Blue
    ["#E57C46", "#E57C46"], # (4) Orange
    ["#BA68C8", "#BA68C8"], # (5) Purple
    ["#6C77BB", "#6C77BB"], # (6) Indigo
    ["#4DD0E1", "#4DD0E1"], # (7) Cyan
    ["#EC7875", "#EC7875"], # (8) Red
    ["#121212", "#121212"], # (9) Black
    ["#ffffff", "#ffffff"], # (10) white
    ["#EC407A", "#EC407A"], # (11) Pink
]

barcolors = [
    ["#212B30", "#212B30"], # (0) main bar bg color,
    ["#263035", "#263035"], # (1) bar bg color 1
    ["#2B353A", "#2B353A"], # (2) bar bg color 2
    ["#303A3F", "#303A3F"], # (3) bar bg color 3
    ["#353F44", "#353F44"], # (4) bar bg color 4
    ["#3A4449", "#3A4449"], # (5) bar bg color 5
    ["#3F494E", "#3F494E"], # (6) bar bg color 6
]

widget_defaults = dict(
    font = 'Ubuntu',
    fontsize = 12,
    padding = 5,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(

        wallpaper='~/Pictures/Wallpapers/Gorilla-Wallpaper2.png',
        wallpaper_mode='fill',

        top=bar.Bar(
            [
                widget.Spacer(
                    length = 10,
                    background = barcolors[6],
                ),

                widget.TextBox(
                    font = 'font-awesome',
                    text = (""),
                    padding = 0,
                    foreground = colors[2],
                    background = barcolors[6],
                ),

                widget.TextBox(
                    text = "◢",
                    background = barcolors[6],
                    foreground = barcolors[5],
                    padding = 0,
                    fontsize = 55
                ),


                widget.GroupBox(
                    font = "Ubuntu Bold",
                    fontsize = 10,
                    margin_y = 3,
                    margin_x = 0,
                    padding_y = 8,
                    active = colors[10],
                    inactive = colors[0],
                    highlight_method = 'line',
                    highlight_color = barcolors[5],
                    this_current_screen_border = colors[11],
                    this_screen_border = colors[11],
                    background = barcolors[5],
                ),

                widget.TextBox(
                    text = "◢",
                    background = barcolors[5],
                    foreground = barcolors[4],
                    padding = 0,
                    fontsize = 55
                ),

                widget.TextBox(
                    font = 'font-awesome',
                    text = (""),
                    padding = 5,
                    foreground = colors[2],
                    background = barcolors[4],
                ),
                widget.CPU(
                    format = '{freq_current}GHz {load_percent}%',
                    background = barcolors[4],
                ),

                widget.TextBox(
                    text = "◢",
                    background = barcolors[4],
                    foreground = barcolors[3],
                    padding = 0,
                    fontsize = 55
                ),

                widget.TextBox(
                    font = 'font-awesome',
                    text = (""),
                    padding = 5,
                    foreground = colors[3],
                    background = barcolors[3],
                ),
                widget.Memory(
                    format = '{MemUsed}M',
                    background = barcolors[3],
                    padding = 5,
                ),

                widget.TextBox(
                    text = "◢",
                    background = barcolors[3],
                    foreground = barcolors[2],
                    padding = 0,
                    fontsize = 55
                ),
                
                widget.CurrentLayoutIcon(
                    scale = .4,
                    foreground = colors[4],
                    background = barcolors[2],
                ),
                widget.CurrentLayout(
                    background = barcolors[2],
                ),

                widget.TextBox(
                    text = "◢",
                    background = barcolors[2],
                    foreground = barcolors[0],
                    padding = 0,
                    fontsize = 55
                ),

                widget.Prompt(),

                widget.TaskList(
                    font = "Ubuntu Bold",
                    fontsize = 12,
                    padding = 5,
                    highlight_method = 'block',
                    rounded = False,
                    background = barcolors[0],
                ),

                widget.TextBox(
                    text = "◢",
                    background = barcolors[0],
                    foreground = barcolors[3],
                    padding = 0,
                    fontsize = 55,
                ),

                widget.TextBox(
                    font = 'font-awesome',
                    text = "",
                    padding_y = 5,
                    foreground = colors[6],
                    background = barcolors[3],
                ),
                widget.Net(
                    interface = 'enp3s0',
                    format = '{interface}',
                    # format = '{down} ↓↑ {up}',
                    background = barcolors[3],
                ),

                widget.TextBox(
                    text = "◢",
                    background = barcolors[3],
                    foreground = barcolors[4],
                    padding = 0,
                    fontsize = 55
                ),

                widget.TextBox(
                    font = 'font-awesome',
                    text = "",
                    padding = 5,
                    foreground = colors[5],
                    background = barcolors[4],
                    mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e pavucontrol')},
                ),
                widget.Volume(
                    background = barcolors[4],
                ),

                widget.TextBox(
                    text = "◢",
                    background = barcolors[4],
                    foreground = barcolors[5],
                    padding = 0,
                    fontsize = 55
                ),

                widget.TextBox(
                    font = 'font-awesome',
                    text = (""),
                    foreground = colors[7],
                    padding = 5,
                    background = barcolors[5],
                ),
                widget.Clock(
                    format = '%H:%M',
                    padding = 5,
                    background = barcolors[5],
                ),

                widget.TextBox(
                    text = "◢",
                    background = barcolors[5],
                    foreground = barcolors[6],
                    padding = 0,
                    fontsize = 55
                ),

                widget.QuickExit(
                    default_text = "",
                    countdown_format = '[ {} seconds ]',
                    max_chars = 10,
                    padding = 10,
                    background = barcolors[6],
                    foreground = colors[8]
                ),
                widget.Systray(
                    icon_size = 15,
                    padding = 10,
                    background = barcolors[6],
                ),
                widget.Spacer(
                    length = 5,
                    background = barcolors[6],
                ),
            ],
            size = 30,
            background = colors[1],
        ),
    ),

    # Screen(

    #     wallpaper='~/Pictures/Wallpapers/Gorilla-Wallpaper.png',
    #     wallpaper_mode='fill',

    #     top=bar.Bar(
    #         [
    #             widget.Spacer(
    #                 length = 10,
    #                 background = barcolors[6],
    #             ),

    #             widget.TextBox(
    #                 font = 'font-awesome',
    #                 text = (""),
    #                 padding = 0,
    #                 foreground = colors[2],
    #                 background = barcolors[6],
    #             ),

    #             widget.TextBox(
    #                 text = "◢",
    #                 background = barcolors[6],
    #                 foreground = barcolors[5],
    #                 padding = 0,
    #                 fontsize = 55
    #             ),


    #             widget.GroupBox(
    #                 font = "Ubuntu Bold",
    #                 fontsize = 10,
    #                 margin_y = 3,
    #                 margin_x = 0,
    #                 padding_y = 8,
    #                 active = colors[10],
    #                 inactive = colors[0],
    #                 highlight_method = 'line',
    #                 highlight_color = barcolors[5],
    #                 this_current_screen_border = colors[11],
    #                 this_screen_border = colors[11],
    #                 background = barcolors[5],
    #             ),

    #             widget.TextBox(
    #                 text = "◢",
    #                 background = barcolors[5],
    #                 foreground = barcolors[4],
    #                 padding = 0,
    #                 fontsize = 55
    #             ),

    #             widget.TextBox(
    #                 font = 'font-awesome',
    #                 text = (""),
    #                 padding = 5,
    #                 foreground = colors[2],
    #                 background = barcolors[4],
    #             ),
    #             widget.CPU(
    #                 format = '{freq_current}GHz {load_percent}%',
    #                 background = barcolors[4],
    #             ),

    #             widget.TextBox(
    #                 text = "◢",
    #                 background = barcolors[4],
    #                 foreground = barcolors[3],
    #                 padding = 0,
    #                 fontsize = 55
    #             ),

    #             widget.TextBox(
    #                 font = 'font-awesome',
    #                 text = (""),
    #                 padding = 5,
    #                 foreground = colors[3],
    #                 background = barcolors[3],
    #             ),
    #             widget.Memory(
    #                 format = '{MemUsed}M',
    #                 background = barcolors[3],
    #                 padding = 5,
    #             ),

    #             widget.TextBox(
    #                 text = "◢",
    #                 background = barcolors[3],
    #                 foreground = barcolors[2],
    #                 padding = 0,
    #                 fontsize = 55
    #             ),
                
    #             widget.CurrentLayoutIcon(
    #                 scale = .4,
    #                 foreground = colors[4],
    #                 background = barcolors[2],
    #             ),
    #             widget.CurrentLayout(
    #                 background = barcolors[2],
    #             ),

    #             widget.TextBox(
    #                 text = "◢",
    #                 background = barcolors[2],
    #                 foreground = barcolors[0],
    #                 padding = 0,
    #                 fontsize = 55
    #             ),

    #             widget.TaskList(
    #                 font = "Ubuntu Bold",
    #                 fontsize = 12,
    #                 padding = 5,
    #                 highlight_method = 'block',
    #                 rounded = False,
    #                 background = barcolors[0],
    #             ),

    #             widget.TextBox(
    #                 text = "◢",
    #                 background = barcolors[0],
    #                 foreground = barcolors[3],
    #                 padding = 0,
    #                 fontsize = 60,
    #             ),

    #             widget.TextBox(
    #                 font = 'font-awesome',
    #                 text = "",
    #                 padding_y = 5,
    #                 foreground = colors[6],
    #                 background = barcolors[3],
    #             ),
    #             widget.Net(
    #                 interface = 'enp3s0',
    #                 format = '{down} ↓↑ {up}',
    #                 background = barcolors[3],
    #             ),

    #             widget.TextBox(
    #                 text = "◢",
    #                 background = barcolors[3],
    #                 foreground = barcolors[4],
    #                 padding = 0,
    #                 fontsize = 45
    #             ),

    #             widget.TextBox(
    #                 font = 'font-awesome',
    #                 text = "",
    #                 padding = 5,
    #                 foreground = colors[5],
    #                 background = barcolors[4],
    #             ),
    #             widget.Volume(
    #                 background = barcolors[4],
    #             ),

    #             widget.TextBox(
    #                 text = "◢",
    #                 background = barcolors[4],
    #                 foreground = barcolors[5],
    #                 padding = 0,
    #                 fontsize = 45
    #             ),

    #             widget.TextBox(
    #                 font = 'font-awesome',
    #                 text = (""),
    #                 foreground = colors[7],
    #                 padding = 5,
    #                 background = barcolors[5],
    #             ),
    #             widget.Clock(
    #                 format='%a %d-%m-%y %H:%M',
    #                 padding = 5,
    #                 background = barcolors[5],
    #             ),
    #         ],
    #         size = 30,
    #         background = colors[1],
    #     ),
    # ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

@hook.subscribe.startup
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
